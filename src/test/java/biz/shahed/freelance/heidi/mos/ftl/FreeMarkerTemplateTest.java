package biz.shahed.freelance.heidi.mos.ftl;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FreeMarkerTemplateTest extends TestCase {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(FreeMarkerTemplateTest.class);

	public FreeMarkerTemplateTest(String testName) {
		super(testName);
	}

	public static Test suite() {
		return new TestSuite(FreeMarkerTemplateTest.class);
	}

	public void testGetSubModule() throws Exception {
		assertEquals("m00i001", "m00i001");
	}
	
}
