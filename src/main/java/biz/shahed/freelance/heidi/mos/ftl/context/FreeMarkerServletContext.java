package biz.shahed.freelance.heidi.mos.ftl.context;

import java.util.Map;

public interface FreeMarkerServletContext {

	public String CONTEXT_PATH 			= "contextPath";
	public String RESOURCES_PATH 		= "resourcesPath";
	public String ICON_PATH				= "iconPath";
	public String IMAGE_PATH			= "imagePath";
	public String CSS_PATH				= "cssPath";
	
	public String JS_PATH				= "jsPath";
	public String EXTJS_PATH			= "extjsPath";
	public String APP_NAME 				= "appName";
	public String APP_TITLE				= "title";
	public String IS_MSIE				= "isMsie";
	
	public String IS_AJAX_REQUEST		= "isAjaxRequest";
	public String IS_DEBUG				= "isDebug";
	public String IS_DEVELOPMENT		= "isDevelopment";
	public String IS_PRODUCTION			= "isProduction";
	public String HAS_OFFLNE_DB			= "hasOfflinedb";
	public String SERVLET_CONTEXT		= "servletContext";
	
	public String FORMATIVE_CONTEXT     = "formativeContext";
	public String APPLICATION_CONTEXT	= "applicationContext";


	public Map<String, Object> getParams();

	public Map<String, Object> putAll(Map<String, Object> params);
	
	public Map<String, Object> putAll4Bug(Map<String, Object> params);
	
	public Map<String, Object> putAll4Dev(Map<String, Object> params);
	
	public Map<String, Object> putAll4Pro(Map<String, Object> params);

	public Map<String, Object> put(String key, Object object);

}
