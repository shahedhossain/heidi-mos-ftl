package biz.shahed.freelance.heidi.mos.ftl.context;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import biz.shahed.freelance.heidi.mos.util.AjaxUtil;
import biz.shahed.freelance.heidi.mos.util.UserAgentUtil;


@Component
public class FreeMarkerServletContextImpl implements FreeMarkerServletContext {
	
	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(FreeMarkerServletContextImpl.class);
			
	@Autowired
    private ServletContext servletContext;
	
	@Autowired
	private FormativeContext formativeContext;
	
	@Autowired 
	private HttpServletRequest httpServletRequest;
	
	@Autowired
	private ApplicationContext applicationContext;	
	
	
	@Override
	public Map<String , Object> getParams(){
				
		Map<String , Object> params = new HashMap<String, Object>();
		params.put(CONTEXT_PATH, 		formativeContext.getContextPath());
		params.put(RESOURCES_PATH, 		formativeContext.getResourcesPath());		
		params.put(ICON_PATH, 			formativeContext.getIconPath());
		params.put(IMAGE_PATH, 			formativeContext.getImagePath());
		params.put(CSS_PATH, 			formativeContext.getCssPath());
		params.put(JS_PATH, 			formativeContext.getJsPath());
		params.put(EXTJS_PATH, 			formativeContext.getExtjsPath());
		params.put(IS_PRODUCTION, 		formativeContext.isProduction());
		params.put(IS_DEVELOPMENT, 		formativeContext.isDevelopment());
		params.put(IS_DEBUG, 			formativeContext.isDebug());
		params.put(IS_MSIE, 			UserAgentUtil.isMsie(httpServletRequest));
		params.put(IS_AJAX_REQUEST, 	AjaxUtil.isAjaxRequest(httpServletRequest));
		params.put(SERVLET_CONTEXT, 	servletContext);
		params.put(FORMATIVE_CONTEXT, 	formativeContext);
		params.put(APPLICATION_CONTEXT, applicationContext);
		
		return params;
	}

	@Override
	public Map<String, Object> put(String key, Object value) {
		Map<String, Object> root = getParams();
		if(!root.containsKey(key)){
			root.put(key, value);
		}
		return root;
	}
	
	@Override
	public Map<String, Object> putAll(Map<String, Object> params) {
		Map<String, Object> root = getParams();
		if(params != null && !params.isEmpty()){
			for(String key:params.keySet()){
				if(!root.containsKey(key)){
					root.put(key, params.get(key));
				}
			}
		}
		return root;
	}
	
	@Override
	public Map<String, Object> putAll4Bug(Map<String, Object> params) {
		Map<String, Object> root = putAll(params);
		root.put(IS_PRODUCTION, 	false);
		root.put(IS_DEVELOPMENT, 	false);
		root.put(IS_DEBUG, 			true);
		return root;
	}
	
	@Override
	public Map<String, Object> putAll4Dev(Map<String, Object> params) {
		Map<String, Object> root = putAll(params);
		root.put(IS_PRODUCTION, 	false);
		root.put(IS_DEVELOPMENT, 	true);
		root.put(IS_DEBUG, 			false);
		return root;
	}
	
	@Override
	public Map<String, Object> putAll4Pro(Map<String, Object> params) {
		Map<String, Object> root = putAll(params);
		root.put(IS_PRODUCTION, 	true);
		root.put(IS_DEVELOPMENT, 	false);
		root.put(IS_DEBUG, 			false);
		return root;
	}
		
}
