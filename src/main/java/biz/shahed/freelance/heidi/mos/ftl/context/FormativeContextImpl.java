package biz.shahed.freelance.heidi.mos.ftl.context;

import javax.servlet.ServletContext;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import biz.shahed.freelance.heidi.mos.util.env.HeidiMos;

@Component
public class FormativeContextImpl implements FormativeContext, ApplicationContextAware {
	
	private ApplicationContext context;
		
	private String contextPath;
	private String resourcesPath;
	private String iconPath;
	private String imagePath;
	private String cssPath;
	private String jsPath;
	private String extjsPath;
	
	private boolean development;
	private boolean production;	
	private boolean debug;
			
	@Autowired
	public FormativeContextImpl(ServletContext servletContext, HeidiMos mcserp){
		contextPath 		= servletContext.getContextPath();
		resourcesPath		= String.format(RESOURCES_PATTERN, contextPath);
		iconPath			= String.format(ICON_PATTERN, resourcesPath);
		imagePath			= String.format(IMAGE_PATTERN, resourcesPath);
		cssPath				= String.format(CSS_PATTERN, resourcesPath);
		jsPath				= String.format(JS_PATTERN, resourcesPath);
		extjsPath			= String.format(EXTJS_PATTERN, resourcesPath);
		
		development			= mcserp.isDevelopment();
		production			= mcserp.isProduction();
		debug				= mcserp.isDebug();
	}
	
	@Override
	public String getActionPath(){
		return contextPath;
	}
	
	@Override
	public String getActionPath(String action){
		if(action != null && !action.isEmpty() && action.startsWith("/")){
			action = action.substring(1);
		}
		return String.format(ACTION_PATTERN, contextPath, action);
	}
	
	@Override
	public ApplicationContext getApplicationContext() {
		return context;
		
	}

	@Override
	public String getContextPath() {
		return contextPath;
	}
	
	@Override
	public String getCssPath() {
		return cssPath;
	}
	
	@Override
	public String getExtjsPath() {
		return extjsPath;
	}
	
	@Override
	public String getIconPath() {
		return iconPath;
	}
	
	@Override
	public String getImagePath() {
		return imagePath;
	}
	
	@Override
	public String getJsPath() {
		return jsPath;
	}
	
	@Override
	public String getResourcesPath() {
		return resourcesPath;
	}
		
	@Override
	public boolean isDebug() {
		return debug;
	}

	@Override
	public boolean isDevelopment() {
		return development;
	}
	
	@Override
	public boolean isProduction() {
		return production;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context = applicationContext;
	}	

}
