package biz.shahed.freelance.heidi.mos.ftl.context;

import org.springframework.context.ApplicationContext;


public interface FormativeContext {

	String ACTION_PATTERN 	= "%s/%s";
	String RESOURCES_PATTERN = "%s/resources";
	String ICON_PATTERN = "%s/icon";
	String IMAGE_PATTERN = "%s/images";
	String CSS_PATTERN = "%s/css";
	String JS_PATTERN = "%s/js";
	String EXTJS_PATTERN = "%s/extjs-4.1";
	
	public String getActionPath();
	
	public String getActionPath(String action);

	public ApplicationContext getApplicationContext();
	
	public String getContextPath();

	public String getCssPath();

	public String getExtjsPath();

	public String getIconPath();

	public String getImagePath();

	public String getJsPath();

	public String getResourcesPath();
		
	public boolean isDebug();
	
	public boolean isDevelopment();
	
	public boolean isProduction();

}
