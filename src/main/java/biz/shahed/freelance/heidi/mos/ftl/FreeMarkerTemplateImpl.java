package biz.shahed.freelance.heidi.mos.ftl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import biz.shahed.freelance.heidi.mos.ftl.context.FreeMarkerServletContext;
import biz.shahed.freelance.heidi.mos.util.FreeMarkerUtil;


import freemarker.template.Configuration;

public class FreeMarkerTemplateImpl implements FreeMarkerTemplate {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(FreeMarkerTemplateImpl.class);

	private Configuration config;
	private FreeMarkerServletContext servletContext;


	public FreeMarkerTemplateImpl(FreeMarkerServletContext servletContext) {
		this.config = FreeMarkerUtil.getConfig(this.getClass(), FTL_ROOT_LOCATION);
		this.servletContext = servletContext;
	}
	
	@Override
	public String parseCSS() {
		Map<String, Object> params = null;
		return parseCSS(params);
	}
	
	@Override
	public String parseCSS(Map<String, Object> params) {
		String css = "heidimos";
		return parseCSS(css, params);
	}

	@Override
	public String parseCSS(String css, Map<String, Object> params) {
		String ftlfile = String.format(FTL_CSS_PATTERN, css);
		Map<String, Object> root = servletContext.putAll(params);
		return FreeMarkerUtil.parse(this.config, ftlfile, root);
	}
	
	@Override
	public String parseHTML() {
		Map<String, Object> params = null;		
		return parseHTML(params);
	}
	
	@Override
	public String parseHTML(Map<String, Object> params) {
		String html = "heidimos";
		return parseHTML(html, params);
	}
	
	@Override
	public String parseHTML(String html, Map<String, Object> params) {
		return parseHTML(html, params, false);
	}
	
	private String parseHTML(String html, Map<String, Object> params, boolean hasOfflinedb) {
		params = putOfflineDBParam(params, hasOfflinedb);
		String ftlfile = String.format(FTL_HTML_PATTERN, html);
		Map<String, Object> root = servletContext.putAll4Pro(params);
		return FreeMarkerUtil.parse(this.config, ftlfile, root);
	}
	
	@Override
	public String parseHTML4All() {
		Map<String, Object> params = null;		
		return parseHTML4All(params);
	}
	
	@Override
	public String parseHTML4All(Map<String, Object> params) {
		String html = "heidimos";
		return parseHTML4All(html, params);
	}
	
	@Override
	public String parseHTML4All(String html, Map<String, Object> params) {
		return parseHTML4All(html, params, false);
	}
	
	private String parseHTML4All(String html, Map<String, Object> params, boolean hasOfflinedb) {
		params = putOfflineDBParam(params, hasOfflinedb);
		String ftlfile = String.format(FTL_HTML_PATTERN, html);
		Map<String, Object> root = servletContext.putAll(params);
		return FreeMarkerUtil.parse(this.config, ftlfile, root);
	}
	
	@Override
	public String parseHTML4Bug() {
		Map<String, Object> params = null;		
		return parseHTML4Bug(params);
	}
	
	@Override
	public String parseHTML4Bug(Map<String, Object> params) {
		String html = "heidimos";
		return parseHTML4Bug(html, params);
	}
	
	@Override
	public String parseHTML4Bug(String html, Map<String, Object> params) {
		return parseHTML4Bug(html, params, false);
	}
	
	private String parseHTML4Bug(String html, Map<String, Object> params, boolean hasOfflinedb) {
		params = putOfflineDBParam(params, hasOfflinedb);
		String ftlfile = String.format(FTL_HTML_PATTERN, html);
		Map<String, Object> root = servletContext.putAll4Bug(params);
		return FreeMarkerUtil.parse(this.config, ftlfile, root);
	}
	
	@Override
	public String parseHTML4Dev() {
		Map<String, Object> params = null;		
		return parseHTML4Dev(params);
	}
	
	@Override
	public String parseHTML4Dev(Map<String, Object> params) {
		String html = "heidimos";
		return parseHTML4Dev(html, params);
	}
	
	@Override
	public String parseHTML4Dev(String html, Map<String, Object> params) {
		return parseHTML4Dev(html, params, false);
	}
	
	private String parseHTML4Dev(String html, Map<String, Object> params, boolean hasOfflinedb) {
		params = putOfflineDBParam(params, hasOfflinedb);
		String ftlfile = String.format(FTL_HTML_PATTERN, html);
		Map<String, Object> root = servletContext.putAll4Dev(params);
		return FreeMarkerUtil.parse(this.config, ftlfile, root);
	}
	
	@Override
	public String parseHTMLWithDB() {
		Map<String, Object> params = null;		
		return parseHTMLWithDB(params);
	}
	
	@Override
	public String parseHTMLWithDB(Map<String, Object> params) {
		String html = "heidimos";
		return parseHTMLWithDB(html, params);
	}	
	
	@Override
	public String parseHTMLWithDB(String html, Map<String, Object> params) {
		return parseHTML(html, params, true);
	}
	
	@Override
	public String parseHTMLWithDB4All() {
		Map<String, Object> params = null;		
		return parseHTMLWithDB4All(params);
	}
	
	@Override
	public String parseHTMLWithDB4All(Map<String, Object> params) {
		String html = "heidimos";
		return parseHTMLWithDB4All(html, params);
	}	
	
	@Override
	public String parseHTMLWithDB4All(String html, Map<String, Object> params) {
		return parseHTML4All(html, params, true);
	}
	
	@Override
	public String parseHTMLWithDB4Bug() {
		Map<String, Object> params = null;		
		return parseHTMLWithDB4Bug(params);
	}
	
	@Override
	public String parseHTMLWithDB4Bug(Map<String, Object> params) {
		String html = "heidimos";
		return parseHTMLWithDB4Bug(html, params);
	}	
	
	@Override
	public String parseHTMLWithDB4Bug(String html, Map<String, Object> params) {
		return parseHTML4Bug(html, params, true);
	}
	
	@Override
	public String parseHTMLWithDB4Dev() {
		Map<String, Object> params = null;		
		return parseHTMLWithDB4Dev(params);
	}
	
	@Override
	public String parseHTMLWithDB4Dev(Map<String, Object> params) {
		String html = "heidimos";
		return parseHTMLWithDB4Dev(html, params);
	}	
	
	@Override
	public String parseHTMLWithDB4Dev(String html, Map<String, Object> params) {
		return parseHTML4Dev(html, params, true);
	}
	
	@Override
	public String parseJS() {
		Map<String, Object> params = null;
		return parseJS(params);
	}
	
	@Override
	public String parseJS(Map<String, Object> params) {
		String js = "heidimos";
		return parseJS(js, params);
	}
	
	@Override
	public String parseJS(String js, Map<String, Object> params) {
		String ftlfile = String.format(FTL_JS_PATTERN, js);
		Map<String, Object> root = servletContext.putAll(params);
		return FreeMarkerUtil.parse(this.config, ftlfile, root);
	}

	private Map<String, Object> putOfflineDBParam(Map<String, Object> params, boolean hasOfflinedb){
		if(params != null && !params.isEmpty()){
			params.put(FreeMarkerServletContext.HAS_OFFLNE_DB, hasOfflinedb);
		}
		return params;
	}	

}
