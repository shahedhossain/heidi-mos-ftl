package biz.shahed.freelance.heidi.mos.ftl.loader;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import biz.shahed.freelance.heidi.mos.ftl.FreeMarkerTemplate;
import biz.shahed.freelance.heidi.mos.ftl.FreeMarkerTemplateImpl;
import biz.shahed.freelance.heidi.mos.ftl.context.FreeMarkerServletContext;


@Component
public class FreeMarkerTemplateLoaderImpl implements FreeMarkerTemplateLoader {

	@SuppressWarnings("unused")
	private static final Logger log = LoggerFactory.getLogger(FreeMarkerTemplateLoaderImpl.class);

	private FreeMarkerTemplate freeMarkerTemplate;

	@Autowired
	public FreeMarkerTemplateLoaderImpl(FreeMarkerServletContext servletContext) {
		this.freeMarkerTemplate = new FreeMarkerTemplateImpl(servletContext);
	}

	@Override
	public String loadCSS() {
		return freeMarkerTemplate.parseCSS();
	}

	@Override
	public String loadCSS(Map<String, Object> params) {
		return freeMarkerTemplate.parseCSS(params);
	}

	@Override
	public String loadCSS(String css, Map<String, Object> params) {
		return freeMarkerTemplate.parseCSS(css, params);
	}
	
	@Override
	public String loadHTML() {
		return freeMarkerTemplate.parseHTML();
	}

	@Override
	public String loadHTML(Map<String, Object> params) {
		return freeMarkerTemplate.parseHTML(params);
	}

	@Override
	public String loadHTML(String html, Map<String, Object> params) {
		return freeMarkerTemplate.parseHTML(html, params);
	}

	@Override
	public String loadHTML4All() {
		return freeMarkerTemplate.parseHTML4All();
	}

	@Override
	public String loadHTML4All(Map<String, Object> params) {
		return freeMarkerTemplate.parseHTML4All(params);
	}

	@Override
	public String loadHTML4All(String html, Map<String, Object> params) {
		return freeMarkerTemplate.parseHTML4All(html, params);
	}
	
	@Override
	public String loadHTML4Bug() {
		return freeMarkerTemplate.parseHTML4Bug();
	}

	@Override
	public String loadHTML4Bug(Map<String, Object> params) {
		return freeMarkerTemplate.parseHTML4Bug(params);
	}

	@Override
	public String loadHTML4Bug(String html, Map<String, Object> params) {
		return freeMarkerTemplate.parseHTML4Bug(html, params);
	}
	
	@Override
	public String loadHTML4Dev() {
		return freeMarkerTemplate.parseHTML4Dev();
	}

	@Override
	public String loadHTML4Dev(Map<String, Object> params) {
		return freeMarkerTemplate.parseHTML4Dev(params);
	}

	@Override
	public String loadHTML4Dev(String html, Map<String, Object> params) {
		return freeMarkerTemplate.parseHTML4Dev(html, params);
	}
	
	@Override
	public String loadHTMLWithDB() {
		return freeMarkerTemplate.parseHTMLWithDB();
	}

	@Override
	public String loadHTMLWithDB(Map<String, Object> params) {
		return freeMarkerTemplate.parseHTMLWithDB(params);
	}

	@Override
	public String loadHTMLWithDB(String html, Map<String, Object> params) {
		return freeMarkerTemplate.parseHTMLWithDB(html, params);
	}

	@Override
	public String loadHTMLWithDB4All() {
		return freeMarkerTemplate.parseHTMLWithDB4All();
	}

	@Override
	public String loadHTMLWithDB4All(Map<String, Object> params) {
		return freeMarkerTemplate.parseHTMLWithDB4All(params);
	}

	@Override
	public String loadHTMLWithDB4All(String html, Map<String, Object> params) {
		return freeMarkerTemplate.parseHTMLWithDB4All(html, params);
	}
	
	@Override
	public String loadHTMLWithDB4Bug() {
		return freeMarkerTemplate.parseHTMLWithDB4Bug();
	}

	@Override
	public String loadHTMLWithDB4Bug(Map<String, Object> params) {
		return freeMarkerTemplate.parseHTMLWithDB4Bug(params);
	}

	@Override
	public String loadHTMLWithDB4Bug(String html, Map<String, Object> params) {
		return freeMarkerTemplate.parseHTMLWithDB4Bug(html, params);
	}
	
	@Override
	public String loadHTMLWithDB4Dev() {
		return freeMarkerTemplate.parseHTMLWithDB4Dev();
	}

	@Override
	public String loadHTMLWithDB4Dev(Map<String, Object> params) {
		return freeMarkerTemplate.parseHTMLWithDB4Dev(params);
	}

	@Override
	public String loadHTMLWithDB4Dev(String html, Map<String, Object> params) {
		return freeMarkerTemplate.parseHTMLWithDB4Dev(html, params);
	}

	@Override
	public String loadJS() {
		return freeMarkerTemplate.parseJS();
	}

	@Override
	public String loadJS(Map<String, Object> params) {
		return freeMarkerTemplate.parseJS(params);
	}

	@Override
	public String loadJS(String js, Map<String, Object> params) {
		return freeMarkerTemplate.parseJS(js, params);
	}

}
