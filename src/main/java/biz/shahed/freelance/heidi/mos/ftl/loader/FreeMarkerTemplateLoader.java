package biz.shahed.freelance.heidi.mos.ftl.loader;

import java.util.Map;

public interface FreeMarkerTemplateLoader {
	
	String loadCSS();

	String loadCSS(Map<String, Object> params);
	
	String loadCSS(String css, Map<String, Object> params);

	String loadHTML();
	
	String loadHTML(Map<String, Object> params);

	String loadHTML(String html, Map<String, Object> params);
	
	String loadHTML4All();
	
	String loadHTML4All(Map<String, Object> params);

	String loadHTML4All(String html, Map<String, Object> params);
	
	String loadHTML4Bug();
	
	String loadHTML4Bug(Map<String, Object> params);

	String loadHTML4Bug(String html, Map<String, Object> params);
	
	String loadHTML4Dev();
	
	String loadHTML4Dev(Map<String, Object> params);

	String loadHTML4Dev(String html, Map<String, Object> params);
	
	String loadHTMLWithDB();
	
	String loadHTMLWithDB(Map<String, Object> params);
	
	String loadHTMLWithDB(String html, Map<String, Object> params);
	
	String loadHTMLWithDB4All();
	
	String loadHTMLWithDB4All(Map<String, Object> params);
	
	String loadHTMLWithDB4All(String html, Map<String, Object> params);
	
	String loadHTMLWithDB4Bug();
	
	String loadHTMLWithDB4Bug(Map<String, Object> params);
	
	String loadHTMLWithDB4Bug(String html, Map<String, Object> params);
	
	String loadHTMLWithDB4Dev();
	
	String loadHTMLWithDB4Dev(Map<String, Object> params);
	
	String loadHTMLWithDB4Dev(String html, Map<String, Object> params);

	String loadJS();

	String loadJS(Map<String, Object> params);
	
	String loadJS(String js, Map<String, Object> params);

}
