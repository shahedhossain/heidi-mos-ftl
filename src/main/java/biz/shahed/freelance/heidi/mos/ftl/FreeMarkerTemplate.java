package biz.shahed.freelance.heidi.mos.ftl;

import java.util.Map;

public interface FreeMarkerTemplate {

	public String FTL_ROOT_LOCATION 	= "/META-INF/ftl/";
	public String FTL_CSS_PATTERN		= "css/%s.css";
	public String FTL_JS_PATTERN		= "js/%s.js";
	public String FTL_HTML_PATTERN		= "html/%s.html";
	
	public String parseCSS();

	public String parseCSS(Map<String, Object> params);
	
	public String parseCSS(String css, Map<String, Object> params);
	
	public String parseHTML();
	
	public String parseHTML(Map<String, Object> params);

	public String parseHTML(String html, Map<String, Object> params);

	public String parseHTML4All();
	
	public String parseHTML4All(Map<String, Object> params);

	public String parseHTML4All(String html, Map<String, Object> params);
	
	public String parseHTML4Bug();
	
	public String parseHTML4Bug(Map<String, Object> params);

	public String parseHTML4Bug(String html, Map<String, Object> params);
	
	public String parseHTML4Dev();
	
	public String parseHTML4Dev(Map<String, Object> params);

	public String parseHTML4Dev(String html, Map<String, Object> params);
	
	public String parseHTMLWithDB();
	
	public String parseHTMLWithDB(Map<String, Object> params);
	
	public String parseHTMLWithDB(String html, Map<String, Object> params);
	
	public String parseHTMLWithDB4All();
	
	public String parseHTMLWithDB4All(Map<String, Object> params);
	
	public String parseHTMLWithDB4All(String html, Map<String, Object> params);
	
	public String parseHTMLWithDB4Bug();
	
	public String parseHTMLWithDB4Bug(Map<String, Object> params);
	
	public String parseHTMLWithDB4Bug(String html, Map<String, Object> params);
	
	public String parseHTMLWithDB4Dev();
	
	public String parseHTMLWithDB4Dev(Map<String, Object> params);
	
	public String parseHTMLWithDB4Dev(String html, Map<String, Object> params);

	public String parseJS();

	public String parseJS(Map<String, Object> params);
	
	public String parseJS(String js, Map<String, Object> params);
	
}
