/**
 * This file is part of Fsl
 * Copyright (c) 2012-2013 Formative Lab Inc
 * Contact:  http://www.formativesoft.com/contact.
 *
 * GNU General Public License Usage
 * This file may be used under the terms of the GNU General Public License version 3.0 as
 * published by the Free Software Foundation and appearing in the file LICENSE included in the
 * packaging of this file.
 * 
 * Please review the following information to ensure the GNU General Public License version 3.0
 * requirements will be met: http://www.gnu.org/copyleft/gpl.html.
 * 
 * If you are unsure which license is appropriate for your use, please contact the sales department
 * at http://www.formativesoft.com/contact.
 * 
 * Build date: 2013-05-25 18:00:00
 **/
 
/*-------------------------------------------------------------------------------------------------*/
 
/**
 * This JavaScript Library use to load JSON data offline mode. This library will works as
 * web database. To reduce simultaneous server request, optimized and maximized peformance
 * of an application.
 *
 * @static
 * @class       db
 * @package     Fsl.db
 * @access      public
 * @version     1.0.0
 * @since       May 25, 2013
 *
 **/
;Fsl.db||(function($) {
	var db = {
		$class      : 'db',
		$package    : 'Fsl.db',
<#list tables.entrySet() as entry>
		${entry.key}  : ${entry.value},
		</#list>
		getInfo     : function(){
			return {$class: db.$class, $package: db.$package};
		}
	};
	$.db = db;
}(Fsl));